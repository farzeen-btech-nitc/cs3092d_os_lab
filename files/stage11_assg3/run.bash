#!/bin/bash
. ../../load_env.bash
shopt -s expand_aliases

rm -f disk.xfs
xi fdisk

xi load --library library.lib

echo "OS"
spl os.spl
if [ $? -ne 0 ]; then
    echo "os.spl compilation failed"
    exit
fi
xi load --os os.xsm

echo "INIT"
expl init.expl
if [ $? -ne 0 ]; then
    echo "init.expl compilation failed"
    exit
fi

xi load --init init.xsm
xi load --exhandler exhandler.xsm

echo "TIMER"
spl timer.spl
if [ $? -ne 0 ]; then
    echo "timer.spl compilation failed"
    exit
fi
xi load --int=timer timer.xsm

echo "INT7"
spl int7.spl
if [ $? -ne 0 ]; then
    echo "int7.spl compilation failed"
    exit
fi
xi load --int=7 int7.xsm

xi load --int=10 int10.xsm

xsm --disk-file disk.xfs --timer 1000 $*
