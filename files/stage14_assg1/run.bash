#!/bin/bash
. ../../load_env.bash
shopt -s expand_aliases

rm -f disk.xfs
xi fdisk

xi load --library $OS_LAB_PROJ_DIR/myexpos/expl/library.lib

spl os.spl
xi load --os os.xsm

expl odd.expl
xi load --init odd.xsm

expl even.expl
xi load --exec even.xsm

expl prime.expl
xi load --exec prime.xsm

expl idle.expl
xi load --idle idle.xsm

xi load --exhandler exhandler.xsm

spl timer.spl
xi load --int=timer timer.xsm

spl int7.spl
xi load --int=7 int7.xsm

spl int10.spl
xi load --int=10 int10.xsm

spl mod5_scheduler.spl
xi load --module 5 mod5_scheduler.xsm

spl mod7_boot.spl
xi load --module 7 mod7_boot.xsm

xsm --disk-file disk.xfs --timer 500 $*
