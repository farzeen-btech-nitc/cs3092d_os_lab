au BufNewFile,BufRead *.xsm set filetype=nasm
au BufNewFile,BufRead *.spl set filetype=c
au BufNewFile,BufRead *.spl ALEDisable
au BufNewFile,BufRead *.expl set filetype=c
au BufNewFile,BufRead *.expl ALEDisable
