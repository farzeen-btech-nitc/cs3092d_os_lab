-- R0 - R7 = Local Vars
    Use Vars starting from R0 for passing to function args
    Use Vars starting from R7 and downwards for local vars, 
    to minimize conflict while passing to functions
    callee must save local vars which it needs to overwrite.
        (Needs more thought on this)

-- R8 - R15 = Saved global vars.
    Must not overwrite inside functions.
