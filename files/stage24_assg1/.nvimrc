au BufNewFile,BufRead *.xsm set filetype=nasm
au BufNewFile,BufRead *.expl set filetype=c
au BufNewFile,BufRead *.expl ALEDisable

let g:ctrlp_working_path_mode = 'c'
