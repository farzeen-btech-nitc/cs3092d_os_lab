#!/bin/bash
. ../../load_env.bash
shopt -s expand_aliases
set -e  # Exit immediately if any of the command fails

rm -f disk.xfs
xi fdisk

xi load --library $OS_LAB_PROJ_DIR/myexpos/expl/library.lib

# From this stage onwards, we don't have any raw xsm files.
# All coding is done in either spl or expl and then compiled to xsm
rm *.xsm;

spl os.spl
xi load --os os.xsm

echo "login.expl"
expl login.expl
xi load --init login.xsm

echo "shell.expl"
expl shell.expl
xi load --shell shell.xsm

expl idle.expl
xi load --idle idle.xsm

spl exhandler.spl
xi load --exhandler exhandler.xsm

spl timer.spl
xi load --int=timer timer.xsm

echo "CONSOLE INT"
spl ./int_console.spl
xi load --int=console int_console.xsm

echo "DISK INT"
spl ./int_disk.spl
xi load --int=disk int_disk.xsm

echo "INT 4 - FILE1"
spl int4_file1.spl
xi load --int=4 int4_file1.xsm

echo "INT 5 - FILE2"
spl int5_file2.spl
xi load --int=5 int5_file2.xsm

echo "INT 6 - READ"
spl int6_read.spl
xi load --int=6 int6_read.xsm

echo "INT 7"
spl int7.spl
xi load --int=7 int7.xsm

echo "INT 8 - FORK"
spl int8_fork.spl
xi load --int=8 int8_fork.xsm

echo "INT 9"
spl int9_exec.spl
xi load --int=9 int9_exec.xsm

echo "INT 10 - EXIT"
spl int10.spl
xi load --int=10 int10.xsm

echo "INT 11 - SYNC"
spl int11_sync.spl
xi load --int=11 int11_sync.xsm

echo "INT 12 - LOGOUT"
spl int12_logout.spl
xi load --int=12 int12_logout.xsm

echo "INT 13 - SEM1"
spl int13_sem1.spl
xi load --int=13 int13_sem1.xsm

echo "INT 14 - SEM2"
spl int14_sem2.spl
xi load --int=14 int14_sem2.xsm

echo "INT 15 - SHUTDOWN"
spl int15_shutdown.spl
xi load --int=15 int15_shutdown.xsm

echo "INT 16 - MULTIUSER"
spl int16_multiuser.spl
xi load --int=16 int16_multiuser.xsm

echo "INT 17 - LOGIN"
spl int17_login.spl
xi load --int=17 int17_login.xsm


echo "MOD 0"
spl mod0_res_mgr.spl
xi load --module 0 mod0_res_mgr.xsm

echo "MOD 1"
spl mod1_process_mgr.spl
xi load --module 1 mod1_process_mgr.xsm

echo "MOD 2"
spl mod2_memory_mgr.spl
xi load --module 2 mod2_memory_mgr.xsm

echo "MOD 3 - FILE"
spl mod3_file_mgr.spl
xi load --module 3 mod3_file_mgr.xsm

echo "MOD 4"
spl mod4_dev_mgr.spl
xi load --module 4 mod4_dev_mgr.xsm

echo "MOD 5"
spl mod5_scheduler.spl
xi load --module 5 mod5_scheduler.xsm

echo "MOD 7"
spl mod7_boot.spl
xi load --module 7 mod7_boot.xsm

echo "touch.expl"
expl touch.expl
xi load --exec touch.xsm

echo "cat.expl"
expl cat.expl
xi load --exec cat.xsm

echo "nano.expl"
expl nano.expl
xi load --exec nano.xsm

echo "ls.expl"
expl ls.expl
xi load --exec ls.xsm

echo "lu.expl"
expl lu.expl
xi load --exec lu.xsm

echo "file.expl"
expl file.expl
xi load --exec file.xsm

echo "putdata.expl"
expl putdata.expl
xi load --exec putdata.xsm

echo "assg1.expl"
expl assg1.expl
xi load --exec assg1.xsm

echo "assg1r.expl"
expl assg1r.expl
xi load --exec assg1r.xsm

echo "assg3.expl"
expl assg3.expl
xi load --exec assg3.xsm

xi load --data numbers.dat

echo "------------------"
xsm --disk-file disk.xfs --timer 1024 $*
