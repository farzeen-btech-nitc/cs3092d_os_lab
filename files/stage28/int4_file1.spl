// R0 - R7  -- Local vars
// R8 - R15 -- Saved global vars
// Saved Global
alias retptr   R8;
alias pcbEntry R9;
alias PID      R10;

// Unsaved Globals
alias syscallNum R15;
alias arg1       R14;
alias arg2       R13;

__INITIALIZE__:
    alias userSP R7;

    userSP = SP;

    PID      = [SYSTEM_STATUS_TABLE + CORE*5 + 1];
    pcbEntry = PROCESS_TABLE + PID*16;

    [pcbEntry + 13] = SP; // UPTR
    SP = [pcbEntry + 11]*512 - 1;

    R0 = userSP - 5;
    R1 = PID;
    call MemTranslate;
    syscallNum = [R0];

    R0 = userSP - 4;
    R1 = PID;
    call MemTranslate;
    arg1 = [R0];

    R0 = userSP - 3;
    R1 = PID;
    call MemTranslate;
    arg2 = [R0];

    // unused args
    // userSP - 2 => arg3

    R0 = userSP - 1;
    R1 = PID;
    call MemTranslate;
    retptr = R0;

    backup;
    R0 = "INT 4 0";
    R1 = ACQUIRE_KERN_LOCK;
    call ACCESS_CONTROL;
    restore;

    alias __unalias__init_1__ R7;


if(syscallNum == INT_CREATE) // Create syscall
then
    [pcbEntry + 9] = INT_CREATE;

    alias filename      R7;
    alias permission    R6;
    alias i             R5;
    alias inodeEntry    R4;

    filename   = arg1;
    permission = arg2;

    if(permission != OPEN_ACCESS && permission != EXCLUSIVE)
    then
        print "W: PERMSN INV";  // KEEP
        print permission;       // KEEP
    endif;

    i = 0;
    while(i<MAX_FILE_NUM)
    do
        inodeEntry = INODE_TABLE + i * 16;

        if([inodeEntry + 1] == filename)
        then
            break;
        endif;

        if([inodeEntry + 1] == -1)
        then
            break;
        endif;

        i = i+1;
    endwhile;

    if(i==MAX_FILE_NUM)
    then
        [retptr] = -1;
        call Exit;
    endif;

    if([inodeEntry + 1] == filename)
    then
        [retptr] = 0;
        call Exit;
    endif;

    alias rootFileEntry R3;
    alias userid R2;
    alias username R1;

    rootFileEntry = ROOT_FILE + i * 8;
    userid        = [pcbEntry + 3];
    username      = [USER_TABLE + userid * 2];

    [inodeEntry + 0]  = DATA;           // TYPE
    [inodeEntry + 1]  = filename;       // FILENAME
    [inodeEntry + 2]  = 0;              // SIZE
    [inodeEntry + 3]  = userid;         // USERID
    [inodeEntry + 4]  = permission;     // PERMISSION
    [inodeEntry + 8]  = -1;             // BLOCK1
    [inodeEntry + 9]  = -1;             // BLOCK2
    [inodeEntry + 10] = -1;             // BLOCK3
    [inodeEntry + 11] = -1;             // BLOCK4

    [rootFileEntry + 0] = filename;     // FILENAME
    [rootFileEntry + 1] = 0;            // SIZE
    [rootFileEntry + 2] = DATA;         // TYPE
    [rootFileEntry + 3] = username;     // USERNAME
    [rootFileEntry + 4] = permission;   // PERMISSION

    [retptr] = 0;
    call Exit;
endif;

if(syscallNum == INT_DELETE) // Delete syscall
then
    [pcbEntry + 9] = INT_DELETE;

    alias filenum R7;

__FIND_INODE_ENTRY__:
    alias _filename R6;
    alias _inodeEntry R5;

    _filename = arg1;
    filenum = 0;
    while(filenum<MAX_FILE_NUM)
    do
        _inodeEntry = INODE_TABLE + filenum * 16;
        if([_inodeEntry + 1] == _filename)
        then
            break;
        endif;
        filenum = filenum + 1;
    endwhile;

    if(filenum == MAX_FILE_NUM)
    then
        [retptr] = 0;
        call Exit;
    endif;

    if([_inodeEntry + 1] != DATA)
    then
        [retptr] = -1;
        call Exit;
    endif;

    if([_inodeEntry + 4] == EXCLUSIVE && [_inodeEntry + 3] != [pcbEntry + 3])
    then
        [retptr] = -1;
    endif;

    alias __unalias__del_01 R6;
    alias __unalias__del_02 R5;

__RELEASE_BLOCKS__:

    // Acquire Inode

    multipush(R7, R8, R9, R10);
    R1 = ACQUIRE_INODE;
    R2 = filenum;
    R3 = PID;
    call RESOURCE_MANAGER;
    multipop(R7, R8, R9, R10);

    // ?? What if res mgr returns -1

    if([FILE_STATUS_TABLE + filenum*4 + 1] != -1)   // Check FILE_OPEN_COUNT = -1
    then
        // File is open, can't delete
        [retptr] = -2;
        call ReleaseAndExit;
    endif;

    alias block      R6;
    alias inodeEntry R5;
    alias i          R4;

    inodeEntry = INODE_TABLE + filenum*16;
    i = 0;
    while(i<4)
    do
        block = [inodeEntry + 8 + i];

        alias bufIndex      R3;
        alias bufTableEntry R2;

        bufIndex = 0;
        while(bufIndex < MAX_BUFFER)
        do
            bufTableEntry = BUFFER_TABLE + bufIndex * 4;
            if([bufTableEntry] == block)
            then
                [bufTableEntry + 0] = -1;   // Invalidate
                [bufTableEntry + 1] = 0;    // Reset DIRTY bit
                [bufTableEntry + 2] = -1;   // Invalidate
                break;
            endif;

            multipush(R3, R4, R5, R6, R7, R8, R9, R10);
            R1 = RELEASE_BLOCK;
            R2 = block;
            call MEMORY_MANAGER;
            multipop(R3, R4, R5, R6, R7, R8, R9, R10);

            bufIndex = bufIndex + 1;
        endwhile;
        i = i+1;
    endwhile;


    alias __unalias__del_11__ R6;
    alias __unalias__del_12__ R5;
    alias __unalias__del_13__ R4;
    alias __unalias__del_14__ R3;
    alias __unalias__del_15__ R2;

__DELETE_END__:
    alias inodeEntry R6;
    alias rootFileEntry R5;

    inodeEntry    = INODE_TABLE + filenum * 16;
    rootFileEntry = ROOT_FILE + filenum * 8;

    // Invalidate inode and root entries
    [inodeEntry + 1] = -1;
    [rootFileEntry]  =  0;


    alias __unalias__del_21__ R6;
    alias __unalias__del_22__ R5;

    [retptr] = 0;
    call ReleaseAndExit;

    ReleaseAndExit:
        multipush(R7, R8, R9, R10);
        R1 = RELEASE_INODE;
        R2 = filenum;
        R3 = PID;
        call RESOURCE_MANAGER;
        multipop(R7, R8, R9, R10);
        call Exit;

endif;



Exit:
    backup;
    R0 = "INT 4 1";
    R1 = RELEASE_LOCK;
    R2 = KERN_LOCK;
    call ACCESS_CONTROL;
    restore;

    [pcbEntry + 9] = 0;
    SP = [pcbEntry + 13];
    ireturn;

MemTranslate:
    alias addr  R0;
    alias mtPID   R1;
    addr = [ PAGE_TABLE_BASE + mtPID*20 + 2*(addr/512) ]*512 + (addr%512);
    return;
