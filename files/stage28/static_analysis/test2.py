import os

def check_file(fname):
    f = open(fname, 'r')
    lines = f.readlines()
    for (i, line) in enumerate(lines):
        if line.find("backup") != -1:
            for l in lines[i:i+7]:
                if l.find("restore") != -1:
                    break
            else:
                print("{:<10} {:<3}: no restore for backup".format(fname, i))
        if line.find("multipush") != -1:
            items = line.strip()[len("multipush"):]
            for l in lines[i:i+7]:
                if l.find("multipop") != -1:
                    items1 = l.strip()[len("multipop"):]
                    if items != items1:
                        print("{:<10} {:<3}: PUSH POP error".format(fname, i), items, items1)
                    break
            else:
                print("{:<10} {:<3}: no multipop for multipush".format(fname, i))



files = [f for f in os.listdir() if f.endswith(".spl")]

for f in files:
    check_file(f)





