alias returnValue  R0;
alias functionNum  R1;
alias arg1         R2;
alias arg2         R3;

if(functionNum == ACQUIRE_TERMINAL)
then
    alias PID  R5;

    alias _processTableEntry  R11;

    PID = arg1;
    _processTableEntry = PROCESS_TABLE + PID*16;

    while([TERMINAL_STATUS_TABLE+0] == 1)
    do
        [_processTableEntry + 4] = WAIT_TERMINAL;


        backup;
        R0 = "MOD 0 A";
        R1 = RELEASE_LOCK;
        R2 = KERN_LOCK;
        call ACCESS_CONTROL;
        restore;

        backup;
        call CONTEXT_SWITCH;
        restore;

        backup;
        R0 = "MOD 0 B";
        R1 = ACQUIRE_KERN_LOCK;
        call ACCESS_CONTROL;
        restore;

    endwhile;

    [TERMINAL_STATUS_TABLE+0] = 1;
    [TERMINAL_STATUS_TABLE+1] = PID;

    return;
endif;

if(functionNum == RELEASE_TERMINAL)
then
    alias _PID                R11;
    alias _i                  R12;
    alias _processTableEntry  R13;

    _PID = arg1;
    _processTableEntry = PROCESS_TABLE + _PID*16;

    if([TERMINAL_STATUS_TABLE+1] != PID)
    then
        returnValue = -1;
        return;
    endif;

    _i = 1;
    while(_i < MAX_PROC_NUM)
    do
        _processTableEntry = PROCESS_TABLE + _i*16;
        if([_processTableEntry+4] == WAIT_TERMINAL)
        then
            [_processTableEntry+4] = READY;
        endif;
        _i = _i+1;
    endwhile;

    [TERMINAL_STATUS_TABLE+0] = 0;
    returnValue = 0;
    return;
endif;

if(functionNum == ACQUIRE_DISK) // FINAL VERSION
then
    alias PID                 R6;
    alias _processTableEntry  R11;

    PID = arg1;
    _processTableEntry = PROCESS_TABLE + PID*16;

    while([DISK_STATUS_TABLE+0] == 1)
    do
        [_processTableEntry + 4] = WAIT_DISK;


        backup;
        R0 = "MOD 0 C";
        R1 = RELEASE_LOCK;
        R2 = KERN_LOCK;
        call ACCESS_CONTROL;
        restore;

        backup;
        call CONTEXT_SWITCH;
        restore;

        backup;
        R0 = "MOD 0 D";
        R1 = ACQUIRE_KERN_LOCK;
        call ACCESS_CONTROL;
        restore;

    endwhile;

    [DISK_STATUS_TABLE+0] = 1;
    [DISK_STATUS_TABLE+4] = PID;

    return;
endif;

if(functionNum == ACQUIRE_SEMAPHORE)
then
    alias _PID  R11;
    alias _i    R12;

    _PID = arg1;

    _i = 0;
    while(_i < MAX_SEM_COUNT)
    do
        if([SEMAPHORE_TABLE + _i*4 + 1] == 0)
        then
            break;
        endif;
        _i = _i + 1;
    endwhile;

    if(_i == MAX_SEM_COUNT)
    then
        returnValue = -1;
    endif;

    [SEMAPHORE_TABLE + _i*4 + 0] = -1;  // Locking PID
    [SEMAPHORE_TABLE + _i*4 + 1] =  1;  // Shared count

    returnValue = _i;
    return;
endif;

if(functionNum == RELEASE_SEMAPHORE)
then
    alias _PID       R11;
    alias _semIdx    R12;
    alias _i         R13;
    alias _pcbEntry  R14;

    _semIdx = arg1;
    _PID    = arg2;

    // If locked by PID, unlock
    if([SEMAPHORE_TABLE + _semIdx*4] == PID)
    then
        [SEMAPHORE_TABLE + _semIdx*4] = -1;

        // Wake up waiting processes
        _i = 0;
        while(_i<16)
        do
            _pcbEntry = PROCESS_TABLE + _i*16;
            if([_pcbEntry + 4] == WAIT_SEMAPHORE && [_pcbEntry + 5] == _semIdx)
            then
                [_pcbEntry + 4] = READY;
            endif;

            _i = _i + 1;
        endwhile;
    endif;

    // Decrement shared count
    [SEMAPHORE_TABLE + _semIdx*4 + 1] = [SEMAPHORE_TABLE + _semIdx*4 + 1] - 1;

    returnValue = 0;
    return;
endif;

if(functionNum == ACQUIRE_INODE)
then
    alias _PID   R11;
    alias _inode R12;

    _inode = arg1;
    _PID = arg2;

    while([FILE_STATUS_TABLE + _inode*4] != -1)
    do
        [PROCESS_TABLE + _PID*16 + 4] = WAIT_FILE;
        [PROCESS_TABLE + _PID*16 + 5] = _inode;

        backup;
        R0 = "MOD 0 E";
        R1 = RELEASE_LOCK;
        R2 = KERN_LOCK;
        call ACCESS_CONTROL;
        restore;

        backup;
        call CONTEXT_SWITCH;
        restore;

        backup;
        R0 = "MOD 0 F";
        R1 = ACQUIRE_KERN_LOCK;
        call ACCESS_CONTROL;
        restore;
    endwhile;

    // If the requested INODE had become invalid while we were waiting
    if([INODE_TABLE + _inode*16 + 1] == -1)
    then
        returnValue = -1;
        return;
    endif;

    [FILE_STATUS_TABLE + _inode*4] = _PID;

    returnValue = 0;
    return;
endif;

if(functionNum == RELEASE_INODE)
then
    alias _PID   R11;
    alias _inode R12;

    _inode = arg1;
    _PID = arg2;

    if([FILE_STATUS_TABLE + _inode*4] != _PID)
    then
        returnValue = -1;
        return;
    endif;

    [FILE_STATUS_TABLE + _inode*4] = -1;

    alias _i   R13;
    alias _pcb R14;
    _i = 0;
    while(_i < MAX_PROC_NUM)
    do
        _pcb = PROCESS_TABLE + _i*16;
        if([_pcb + 4] == WAIT_FILE && [_pcb + 5] == _inode)
        then
            [_pcb + 4] = READY;
        endif;

        _i = _i + 1;
    endwhile;

    returnValue = 0;
    return;
endif;

// BUFFER

if(functionNum == ACQUIRE_BUFFER)
then
    alias _PID   R11;
    alias _buf R12;

    _buf = arg1;
    _PID = arg2;

    while([BUFFER_TABLE + _buf*4 + 2] != -1)
    do
        [PROCESS_TABLE + _PID*16 + 4] = WAIT_BUFFER;
        [PROCESS_TABLE + _PID*16 + 5] = _buf;

        backup;
        R0 = "MOD 0 G";
        R1 = RELEASE_LOCK;
        R2 = KERN_LOCK;
        call ACCESS_CONTROL;
        restore;

        backup;
        call CONTEXT_SWITCH;
        restore;

        backup;
        R0 = "MOD 0 H";
        R1 = ACQUIRE_KERN_LOCK;
        call ACCESS_CONTROL;
        restore;

    endwhile;

    [BUFFER_TABLE + _buf*4 + 2] = _PID;

    returnValue = 0;
    return;
endif;

if(functionNum == RELEASE_BUFFER)
then
    alias _PID   R11;
    alias _buf R12;

    _buf = arg1;
    _PID = arg2;

    if([BUFFER_TABLE + _buf*4 + 2] != _PID)
    then
        print "E: BUF PID";     // KEEP
        returnValue = -1;
        return;
    endif;

    [BUFFER_TABLE + _buf*4 + 2] = -1;

    alias _i   R13;
    alias _pcb R14;
    _i = 0;
    while(_i < MAX_PROC_NUM)
    do
        _pcb = PROCESS_TABLE + _i*16;
        if([_pcb + 4] == WAIT_BUFFER && [_pcb + 5] == _buf)
        then
            [_pcb + 4] = READY;
        endif;

        _i = _i + 1;
    endwhile;

    returnValue = 0;
    return;
endif;

return;
