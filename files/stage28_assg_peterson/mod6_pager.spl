define USER_PID_START 3; // 0: Idle, 1: Init, 2: Shell <- Skip these
define USER_PID_END  13; // 14: IDLE2, 15: Swapper <- Skip these

define HEAP_PAGES_START  2;
define HEAP_PAGES_END    3;
define CODE_PAGES_START  4;
define CODE_PAGES_END    7;
define STACK_PAGES_START 8;
define STACK_PAGES_END   9;

alias returnValue R0;
alias functionNum R1;
alias arg1 R2;

// call SwappedCountSanityCheck;

if(functionNum == SWAP_OUT)
then
    alias PID R5;
    alias i   R6;
    alias procTableEntry R7;

    PID = arg1;

    // Find a process with state == WAIT_PROCESS
    i=USER_PID_START;
    while(i<=USER_PID_END)
    do
        procTableEntry = PROCESS_TABLE + i*16;
        if([procTableEntry + 4] == WAIT_PROCESS && [procTableEntry + 6] == 0)
        then
            break;
        endif;

        i = i+1;
    endwhile;

    // Find a process with state == WAIT_SEMAPHORE
    if(i>USER_PID_END)
    then
        i=USER_PID_START;
        while(i<=USER_PID_END)
        do
            procTableEntry = PROCESS_TABLE + i*16;
            if([procTableEntry + 4] == WAIT_SEMAPHORE && [procTableEntry + 6] == 0)
            then
                break;
            endif;

            i = i+1;
        endwhile;
    endif;

    // Among the swappable processes, get the one with highest tick
    if(i>USER_PID_END)
    then
        alias highestTickPID R15;
        alias highestTick    R14;
        alias procStatus     R13;

        highestTickPID = USER_PID_END + 1; // Sentinel for no process found
        highestTick = -1;

        i=USER_PID_START;
        while(i<=USER_PID_END)
        do
            procTableEntry = PROCESS_TABLE + i*16;
            procStatus = [procTableEntry + 4];
            if(procStatus == RUNNING || procStatus == TERMINATED || procStatus == ALLOCATED ||
               [procTableEntry + 6] == 1)
            then
                i = i + 1;
                continue;
            endif;

            if([procTableEntry] > highestTick)
            then
                highestTickPID = i;
                highestTick = [procTableEntry];
            endif;

            i = i+1;
        endwhile;

        i = highestTickPID;
        procTableEntry = PROCESS_TABLE + i*16;
    endif;

    // No swappable process found
    if(i > USER_PID_END)
    then
        [SYSTEM_STATUS_TABLE + 5] = 0;  // PAGING STATUS
        return;
    endif;

    [procTableEntry] = 0;   // Reset TICK

alias procPTBR   R8;
alias pageIdx    R9;
alias phyPage    R10;
alias diskBlock  R11;

    procPTBR = [procTableEntry + 14];

    pageIdx = HEAP_PAGES_START;
    while(pageIdx<=STACK_PAGES_END)
    do
        phyPage = [procPTBR + pageIdx*2];
        if(phyPage == -1)
        then
            goto __SWAP_OUT_LOOP_END;
        endif;

        if(pageIdx >= HEAP_PAGES_START && pageIdx <= HEAP_PAGES_END &&
            [MEMORY_FREE_LIST + phyPage] > 1)
        then
            // If heap page, swap only if it is unshared
            goto __SWAP_OUT_LOOP_END;
        endif;

        if(pageIdx >= CODE_PAGES_START && pageIdx <= CODE_PAGES_END)
        then
            goto __SWAP_OUT_LOOP_RELEASE_PAGE;
        endif;

        // Get disk page to swap to
        multipush(R5, R6, R7, R8, R9, R10, R11);
        R1 = GET_SWAP_BLOCK;
        call MEMORY_MANAGER;
        multipop(R5, R6, R7, R8, R9, R10, R11);
        diskBlock = R0;


        [DISK_MAP_TABLE + i*10 + pageIdx] = diskBlock;

        backup;
        R1 = DISK_STORE;
        R2 = PID; // PID
        R3 = phyPage;
        R4 = diskBlock;
        call DEVICE_MANAGER;
        restore;

    __SWAP_OUT_LOOP_RELEASE_PAGE:
        // Note:
        // RELEASE_PAGE  decrements the shared count of a page.
        // It deallocates only if shared count is zero.
        backup;
        R1 = RELEASE_PAGE;
        R2 = [procPTBR + pageIdx*2];
        call MEMORY_MANAGER;
        restore;

        [procPTBR + pageIdx*2     ] = -1;
        [procPTBR + pageIdx*2 + 1 ] = "0000";

    __SWAP_OUT_LOOP_END:
        pageIdx = pageIdx + 1;
    endwhile;

    // Set process as swapped out.
    [procTableEntry + 6] = 1;

    // Increment swapped out count.
    [SYSTEM_STATUS_TABLE + 4] = [SYSTEM_STATUS_TABLE + 4] + 1;
    [SYSTEM_STATUS_TABLE + 5] = 0;  // Swapping Status

    return;
endif;

if(functionNum == SWAP_IN)
then
    alias PID R5;
    alias i   R6;
    alias procTableEntry R7;

    PID = arg1;

    // Among the swapped out processes which are ready, get the one with highest tick
        alias highestTickPID R15;
        alias highestTick    R14;

        highestTickPID = USER_PID_END + 1; // Sentinel for no process found
        highestTick = -1;

        i=USER_PID_START;
        while(i<=USER_PID_END)
        do
            procTableEntry = PROCESS_TABLE + i*16;
            if(!([procTableEntry + 6] == 1 && [procTableEntry + 4] == READY))
            then
                i = i + 1;
                continue;
            endif;

            if([procTableEntry] > highestTick)
            then
                highestTickPID = i;
                highestTick = [procTableEntry];
            endif;

            i = i+1;
        endwhile;

        i = highestTickPID;
        procTableEntry = PROCESS_TABLE + i*16;

    // No swapped out, ready process found
    if(i > USER_PID_END)
    then
        [SYSTEM_STATUS_TABLE + 5] = 0;  // PAGING STATUS
        return;
    endif;

    [procTableEntry] = 0;  // Reset TICK

alias procPTBR   R8;
alias pageIdx    R9;
alias phyPage    R10;
alias diskBlock  R11;

    procPTBR = [procTableEntry + 14];
    pageIdx = HEAP_PAGES_START;
    while(pageIdx<=STACK_PAGES_END)
    do
        diskBlock = [DISK_MAP_TABLE + i*10 + pageIdx];
        phyPage = [procPTBR + pageIdx*2];

        if(phyPage != -1 || diskBlock == -1)
        then
            goto __SWAP_IN_LOOP_END;
        endif;

        if(pageIdx >= CODE_PAGES_START && pageIdx <= CODE_PAGES_END)
        then
            // Don't bring in code pages now, load on demand through page faults
            goto __SWAP_IN_LOOP_END;
        endif;

        // Get free page to swap back into
        multipush(R5, R6, R7, R8, R9, R10, R11);
        R1 = GET_FREE_PAGE;
        call MEMORY_MANAGER;
        multipop(R5, R6, R7, R8, R9, R10, R11);
        phyPage = R0;


        // Note: we are not loading code pages. So set as Read-Write.
        [procPTBR + pageIdx*2     ] = phyPage;
        [procPTBR + pageIdx*2 + 1 ] = "1110";

        backup;
        R1 = DISK_LOAD;
        R2 = PID; // PID
        R3 = phyPage;
        R4 = diskBlock;
        call DEVICE_MANAGER;
        restore;

    __SWAP_IN_LOOP_RELEASE_BLOCK:
        // Note: we are not loading code pages. So release the corresponding disk block.
        backup;
        R1 = RELEASE_BLOCK;
        R2 = diskBlock;
        call MEMORY_MANAGER;
        restore;

        // Note: we are not loading code pages. So invalidate disk map table entry.
        [DISK_MAP_TABLE + i*10 + pageIdx] = -1;

    __SWAP_IN_LOOP_END:
        pageIdx = pageIdx + 1;
    endwhile;

    // Set process as not swapped out
    [procTableEntry + 6] = 0;
    // Decrement swapped out count
    [SYSTEM_STATUS_TABLE + 4] = [SYSTEM_STATUS_TABLE + 4] - 1;
    [SYSTEM_STATUS_TABLE + 5] = 0;  // Swapping Status

    return;
endif;

// SwappedCountSanityCheck:
//     backup;
//     R14 = 0;
//     R15 = 0;
//     while(R15 < MAX_PROC_NUM)
//     do
//         if([PROCESS_TABLE + R15*16 + 6] == 1)
//         then
//             R14 = R14 + 1;
//         endif;
//         R15 = R15 + 1;
//     endwhile;
//
//     if(R14 != [SYSTEM_STATUS_TABLE + 4])
//     then
//         print "SW COUNT MISMATCH";
//         print R14;
//         print [SYSTEM_STATUS_TABLE + 4];
//     endif;
//     restore;
//     return;
