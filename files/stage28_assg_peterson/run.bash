#!/bin/bash
. ../../load_env.bash
shopt -s expand_aliases
set -e  # Exit immediately if any of the command fails

rm -f disk.xfs
nxi fdisk

nxi load --library $OS_LAB_PROJ_DIR/myexpos/expl/library.lib

# We don't have any raw xsm files.
# All coding is done in either nespl or expl and then compiled to xsm
rm -f *.xsm;

echo "os.spl"
nespl os.spl
nxi load --os=primary os.xsm

echo "os2.spl"
nespl os2.spl
nxi load --os=secondary os2.xsm

echo "login.expl"
expl login.expl
nxi load --init login.xsm

echo "shell.expl"
expl shell.expl
nxi load --shell shell.xsm

echo "idle.expl"
expl idle.expl
nxi load --idle idle.xsm

echo "exhandler.spl"
nespl exhandler.spl
nxi load --exhandler exhandler.xsm

echo "timer.spl"
nespl timer.spl
nxi load --int=timer timer.xsm

echo "CONSOLE INT"
nespl ./int_console.spl
nxi load --int=console int_console.xsm

echo "DISK INT"
nespl ./int_disk.spl
nxi load --int=disk int_disk.xsm

echo "INT 4 - FILE1"
nespl int4_file1.spl
nxi load --int=4 int4_file1.xsm

echo "INT 5 - FILE2"
nespl int5_file2.spl
nxi load --int=5 int5_file2.xsm

echo "INT 6 - READ"
nespl int6_read.spl
nxi load --int=6 int6_read.xsm

echo "INT 7"
nespl int7.spl
nxi load --int=7 int7.xsm

echo "INT 8 - FORK"
nespl int8_fork.spl
nxi load --int=8 int8_fork.xsm

echo "INT 9"
nespl int9_exec.spl
nxi load --int=9 int9_exec.xsm

echo "INT 10 - EXIT"
nespl int10.spl
nxi load --int=10 int10.xsm

echo "INT 11 - SYNC"
nespl int11_sync.spl
nxi load --int=11 int11_sync.xsm

echo "INT 12 - LOGOUT"
nespl int12_logout.spl
nxi load --int=12 int12_logout.xsm

echo "INT 13 - SEM1"
nespl int13_sem1.spl
nxi load --int=13 int13_sem1.xsm

echo "INT 14 - SEM2"
nespl int14_sem2.spl
nxi load --int=14 int14_sem2.xsm

echo "INT 15 - SHUTDOWN"
nespl int15_shutdown.spl
nxi load --int=15 int15_shutdown.xsm

echo "INT 16 - MULTIUSER"
nespl int16_multiuser.spl
nxi load --int=16 int16_multiuser.xsm

echo "INT 17 - LOGIN"
nespl int17_login.spl
nxi load --int=17 int17_login.xsm


echo "MOD 0"
nespl mod0_res_mgr.spl
nxi load --module 0 mod0_res_mgr.xsm

echo "MOD 1"
nespl mod1_process_mgr.spl
nxi load --module 1 mod1_process_mgr.xsm

echo "MOD 2"
nespl mod2_memory_mgr.spl
nxi load --module 2 mod2_memory_mgr.xsm

echo "MOD 3 - FILE"
nespl mod3_file_mgr.spl
nxi load --module 3 mod3_file_mgr.xsm

echo "MOD 4"
nespl mod4_dev_mgr.spl
nxi load --module 4 mod4_dev_mgr.xsm

echo "MOD 5"
nespl mod5_scheduler.spl
nxi load --module 5 mod5_scheduler.xsm

echo "MOD 6"
nespl mod6_pager.spl
nxi load --module 6 mod6_pager.xsm

echo "MOD 7"
nespl mod7_boot.spl
nxi load --module 7 mod7_boot.xsm

echo "MOD 8"
nespl mod8_access_control.spl
nxi load --module 8 mod8_access_control.xsm

# Load user programs
for i in *.expl; do
    if [[ "$i" == "login.expl" || "$i" == "shell.expl" || "$i" == "idle.expl" ]];
    then
        continue;
    fi;
    echo "-- $i";
    expl "$i";
    nxi load --exec "${i%.expl}.xsm";
done;


echo "------------------"
nexsm --disk-file disk.xfs --timer 1000 --disk 20 $*
