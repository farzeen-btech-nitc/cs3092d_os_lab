import os

def has_backup(lines, i):
    buf = lines[max(i-6, 0):i]
    for l in buf:
        if l.find("backup") >= 0 or l.find("multipush") >= 0 or l.find("call Save") >= 0:
            return True
    return False

def has_restore(lines, i):
    buf = lines[i:i+5]
    for l in buf:
        if l.find("restore") >= 0 or l.find("multipop") >= 0 or l.find("call Restore") >= 0:
            return True
    return False

def check_file(fname):
    f = open(fname, 'r')
    lines = f.readlines()
    for (i, line) in enumerate(lines):
        if line.find("call ") >= 0:
            j = line.find("call ")
            t = line[j:].split()[1]
            if t.upper() != t:
                continue
            if not has_backup(lines, i):
                print("{:<20}:{:<5}\tNO BACKUP \t- `{}`".format(fname, i+1, line.strip()))
            if not has_restore(lines, i):
                print("{:<20}:{:<5}\tNO RESTORE \t- `{}`".format(fname, i+1, line.strip()))


files = [f for f in os.listdir() if f.endswith(".spl")]

for f in files:
    check_file(f)





