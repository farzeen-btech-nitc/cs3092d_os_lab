import os

def check_file(fname):
    f = open(fname, 'r')
    lines = f.readlines()

    sp_set = False
    ops = ["multipush", "multipop", "backup", "restore", "return", "ireturn"]

    for (i, line) in enumerate(lines):
        if line.find("SP = ") != -1:
            sp_set = True
            if line.find("+ 13") != -1:
                sp_set = False
        if not sp_set:
            for o in ops:
                if line.find(o) != -1:
                    print("{:<16}:{:<3} {} before SP set".format(fname, i, o))

files = [f for f in os.listdir() if f.endswith(".spl")]

for f in files:
    if f.startswith("int"):
        check_file(f)
