#!/bin/bash
. ../../load_env.bash
shopt -s expand_aliases

rm -f disk.xfs
xi fdisk
spl os.spl
xi load --os os.xsm
xi load --int=10 int10.xsm
xi load --exhandler exhandler.xsm
xi load --init init.xsm
xsm --disk-file disk.xfs --debug --timer 0
